import { ActivityRingData } from "./ActivityRings";
const Pie = require("paths-js/pie");

const PieFactory = {
  create: (data: ActivityRingData[], height: number, rad: number, fill?: number[]) => {
    return data.map((ring: ActivityRingData, idx: number) => {
      if (ring.value <= 0 && idx != 3) ring.value = 0.0001;
      else if (ring.value >= 1 && idx != 3) ring.value = 0.999;

      const pieData = fill || [ring.value, 1 - ring.value];

      if (idx == 0) {
        const r = ((height / 2 - rad) / data.length) * (data.length - 1 - 1) + rad;
        return Pie({
          r,
          R: r,
          data: pieData,
          center: [0, 0],
          accessor(x) {
            return x;
          }
        });
      } else if (idx == 3) {
        const r = ((height / 2 - rad) / data.length) * (data.length - 2 - 1) + rad;
        var pieData2 = pieData;
        if (ring.value <= 0) pieData2 = fill || [-ring.value, 1 + ring.value];
        else if (ring.value >= 1) pieData2 = fill || [0.999, 1 - 0.999];
        return Pie({
          r,
          R: r,
          data: pieData2,
          center: [0, 0],
          accessor(x) {
            return x;
          }
        });
      } else {
        const pieData = fill || [ring.value, 1 - ring.value];
        const r = ((height / 2 - rad) / data.length) * (data.length - idx - 1) + rad;
        return Pie({
          r,
          R: r,
          data: pieData,
          center: [0, 0],
          accessor(x) {
            return x;
          }
        });
      }
    });
  }
};

export default PieFactory;
