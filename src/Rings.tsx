import * as React from "react";
import { ActivityRingsData } from "./ActivityRings";
import { ThemeColors } from "./Themes";
import { G, Path } from "react-native-svg";

type Pie = {
  // pie-js does not support TS
  curves: any[];
};

type RingsProps = {
  size: number;
  pie: Pie[];
  data: ActivityRingsData;
  theme: ThemeColors;
  opacity: boolean;
};

const Rings = ({ size, pie, data, theme, opacity }: RingsProps) => {
  const alpha = opacity ? "50" : "";
  var flip = false;
  return (
    <G>
      {pie.map((ring: Pie, idx: number) => {
        const dataObj = data[idx];
        const color = opacity ? dataObj.backgroundColor || dataObj.color : dataObj.color;
        const ringColor = color || theme.RingColors[idx];
        // check decimals between 0 and 1
        if (idx == 3 && dataObj.value < 0) {
          flip = true;
          //dataObj.value = -(dataObj.value);
        } else if (dataObj.value <= 0) dataObj.value = 0.0001;
        else if (dataObj.value >= 1) dataObj.value = 0.999;
        return (
          <Path
            transform={flip ? { scaleX: -1 } : {}}
            key={`r_${idx}`}
            strokeLinecap="round"
            strokeLinejoin="round"
            d={ring.curves[0].sector.path.print()}
            stroke={`${ringColor}${(idx == 1 || idx == 2) && opacity ? "00" : alpha}`}
            strokeWidth={idx > 1 ? size - 3 : size + 3}
          />
        );
      })}
    </G>
  );
};

export default Rings;
